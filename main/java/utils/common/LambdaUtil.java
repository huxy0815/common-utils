package main.java.utils.common;

import java.util.function.Consumer;
import java.util.function.Function;

public class LambdaUtil {
	public static <T, R> Function<T, R> wrap(UnwrappedFunction<T, R> unwrapped) {
		return t -> {
			try {
				return unwrapped.apply(t);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}

	public static <T> Consumer<T> wrap(UnwrappedConsumer<T> unwrapped) {
		return t -> {
			try {
				unwrapped.accept(t);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}

	public interface UnwrappedConsumer<T> {
		void accept(T t) throws Exception;
	}

	public interface UnwrappedFunction<T, R> {
		R apply(T t) throws Exception;
	}
}
