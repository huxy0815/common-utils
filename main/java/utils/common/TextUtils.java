package main.java.utils.common;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class TextUtils {
	public static String DOUBLE_PATTERN = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?";
	public static String FLOAT_PATTERN = "[-+]?[0-9]*\\.?[0-9]+";

	public static boolean containsNumeric(String input) {
		for (int i = 0; i < input.length(); i++) {
			if (Character.isDigit(input.charAt(i))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 计算字符在给定字符串出现的次数
	 */
	public static int countChar(String input, char ch) {
		return (int) IntStream.range(0, input.length())
				.filter(i -> ch == input.charAt(i))
				.count();
	}

	public static List<String> extractAllByRegex(String regex, String... inputs) {
		List<String> values = new ArrayList<>();
		Pattern pattern = Pattern.compile(regex);
		for (String input : inputs) {
			for (Matcher matcher = pattern.matcher(input); matcher.find(); ) {
				values.add(matcher.group());
			}
		}
		return values;
	}

	public static String extractFirstByRegex(String regex, String... inputs) {
		Pattern pattern = Pattern.compile(regex);
		for (String input : inputs) {
			Matcher matcher = pattern.matcher(input);
			if (matcher.find()) {
				return matcher.group();
			}
		}
		return null;
	}

	public static String removeBlank(String origin) {
		return origin.replaceAll("[ \t\r\n]", "");
	}
}
