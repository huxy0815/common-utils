package main.java.utils.common;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ListUtils {
	public static <T> Iterable<T> reversed(List<T> t) {
		ListIterator<T> iterator = t.listIterator(t.size());
		return () -> new Iterator<T>() {
			@Override
			public boolean hasNext() {
				return iterator.hasPrevious();
			}

			@Override
			public T next() {
				return iterator.previous();
			}
		};
	}
}
