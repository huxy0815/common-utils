package main.java.utils.hxy.thread.thread.batch;

import com.google.common.collect.Lists;
import com.jfinal.log.Logger;
import com.wanma.framework.util.thread.ISinglePoolFactory;
import com.wanma.framework.util.thread.IThreadPool;

import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

/**
 * 批量需要执行的Runable
 *
 * @author kreo
 */
public class BatchTaskRunable implements Runnable {
    private static Logger log = Logger.getLogger(BatchTaskRunable.class);
    private Runnable mainTask; // 主执行任务
    private List<BatchTask> prepareTasks; // 准备任务, 比如准备任务全部执行完毕,才执行主任务

    public BatchTaskRunable setMainTask(Runnable mainTask) {
        this.mainTask = mainTask;
        return this;
    }

    public BatchTaskRunable setPrepareTasks(List<BatchTask> prepareTasks) {
        this.prepareTasks = prepareTasks;
        return this;
    }

    @Override
    public void run() {
        if (prepareTasks != null && prepareTasks.size() > 0) {
            int n = prepareTasks.size();
            final CountDownLatch latch = new CountDownLatch(n);
            for (int i = 0; i < n; i++) {
                IThreadPool.addTask(prepareTasks.get(i).setCountDownLatch(latch));
            }
            try {
                latch.await();
            } catch (InterruptedException e) {
                log.error("线程已经被强制结束.", e);
            }
        }
        if (this.mainTask != null) {
            this.mainTask.run();
        }
    }

    public static void main(String[] args) {
        dynamicTaskTest();
    }

    public static void dynamicTaskTest() {
        Random r = new Random();
        List<BatchTask> batchTasks = Lists.newArrayList();
        for (int i = 0; i < 20; i++) {
            // List<Record>
            final int j = i;
            batchTasks.add(BatchTask.build(() -> {
                int st = r.nextInt(500);
                try {
                    Thread.sleep(st); // 随机停顿0-0.5秒
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("打印:" + j);
            }));
        }

        ISinglePoolFactory.addTask("TEST1", new BatchTaskRunable().setMainTask(() -> {
            // 设置主任务
            System.out.println("执行完毕");
        }).setPrepareTasks(
                batchTasks
                //
        ));
    }

    public static void staticTaskTest() {
        Random r = new Random();
        // 跑20次
        for (int i = 0; i < 200; i++) {
            final int j = i;
            ISinglePoolFactory.addTask("TEST", new BatchTaskRunable().setMainTask(() -> {
                // 设置主任务
                System.out.println("执行完毕" + j);
            }).setPrepareTasks(
                    Lists.newArrayList(
                            // 设置任务1
                            BatchTask.build(() -> {
                                int st = r.nextInt(500);
                                try {
                                    Thread.sleep(st); // 随机停顿0-0.5秒
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("TASK1_RUN" + j + ":" + st);
                            }),
                            // 设置任务2
                            BatchTask.build(() -> {
                                int st = r.nextInt(500);
                                try {
                                    Thread.sleep(st); // 随机停顿0-0.5秒
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("TASK2_RUN" + j + ":" + st);
                            })
                    )
            ));
        }
    }
}
