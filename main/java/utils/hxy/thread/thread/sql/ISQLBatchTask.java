package main.java.utils.hxy.thread.thread.sql;

import com.wanma.framework.util.thread.IThreadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

/**
 * 注意,暂时不支持参数 , 只能执行SQL , 和ISQLTask的区别是 , 该类本身自带了一个即时销毁的线程池,
 * 传入多个SQL , 并等SQL全部执行完之后,在进行完成动作
 * 
 * @ClassName StatmentTask
 * @Description
 * 
 */
public class ISQLBatchTask implements Callable<List<ISQLObject>> {
	// private final static Logger log = Logger.getLogger(ISQLBatchTask.class);
	private List<ISQLObject> isqlObjects = new ArrayList<>();
	// private ExecutorService pool;

	public ISQLBatchTask(List<ISQLObject> isqlObjects) {
		this.isqlObjects = isqlObjects;
	}

	public List<ISQLObject> call() throws Exception {
		// log.info("线程开始:" + System.currentTimeMillis());
		// pool = Executors.newCachedThreadPool();
		final CountDownLatch latch = new CountDownLatch(isqlObjects.size());
		for (ISQLObject isqlObject : isqlObjects) {
			IThreadPool.addCallable(new ISQLTask(isqlObject, latch));
		}
		latch.await();
		// pool.shutdown();
		// log.info("线程结束:" + System.currentTimeMillis());
		return isqlObjects;
	}
}
