/**
 *
 *
 * +----------------------------------------------------------------------------------------------+
 * |Date               |  Version  |Author             |Description                              |
 * |==========+=======+==============+===================|
 * |2018年1月28日     |  1.0.0       | kreo                 |Initial                                       |
 * +----------------------------------------------------------------------------------------------+
 */
package main.java.utils.hxy.thread.thread.sql;

import com.wanma.framework.util.IJSON;

/**
 * 
 * @ClassName StatmentObject
 * @Description
 * 
 */
public class ISQLObject {
	private String mark;// SQL标识,写Log时区分使用
	private String dbName;// 执行SQL的域
	private String sql;// 执行SQL
	private Object[] para; // 执行SQL的参数
	private Integer count; // 执行数量
	private Integer status; // 执行状态
	private Object result; // 执行结果

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public Object[] getPara() {
		return para;
	}

	public void setPara(Object[] para) {
		this.para = para;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return IJSON.toJSONString(this);
	}

	public static ISQLObject build(String mark, String dbName, String sql, Object[] para) {
		ISQLObject isqlObject = new ISQLObject();
		isqlObject.setMark(mark);
		isqlObject.setDbName(dbName);
		isqlObject.setSql(sql);
		isqlObject.setPara(para);
		return isqlObject;
	}

	public static ISQLObject build(String mark, String dbName, String sql) {
		return build(mark, dbName, sql, null);
	}

	public static final int ERROR = 0;
	public static final int SUCCESS = 1;
	public static final int WARN = 2;

}
