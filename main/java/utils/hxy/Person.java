package main.java.utils.hxy;

import java.util.stream.Stream;

/**
 * @author hxy
 * @version V1.0
 * @Package utils.hxy
 * @date 2019/10/23 14:10
 * @Copyright
 */
public class Person {
    private int id;
    private String name;
    public Person(int id, String name) {
        this.id = id;
        this.name = name;
    }
    // 创建一个person进行回调
    public static void create(Integer id, String name, PersonCallback personCallback)
    {
        Person person = new Person(id, name);
        personCallback.callback(person);
    }
    public static void main(String[] args) {
        Person.create(1, "周瑜", new PersonCallback() {
            public void callback(Person person) {
                System.out.println("111.");
            }
        });
        Person.create(2, "老师", new PersonCallback() {
            public void callback(Person person) {
                System.out.println("2222");
            }
        });
        Person.create(1, "333", (Person person) -> {System.out.println("3333");});


        Stream<String> stream = Stream.<String>of(new String[]{"a", "b", "c"});
        stream.map(String::toUpperCase).forEach(System.out::println);
    }
}
