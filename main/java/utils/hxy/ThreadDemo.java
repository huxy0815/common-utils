package main.java.utils.hxy;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author hxy
 * @version V1.0
 * @Package utils.hxy
 * @date 2019/10/9 10:51
 * @Copyright
 */
public class ThreadDemo {
    public static void main(String[] args) {
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {
            final int index = i;
            try {
                Thread.sleep(  1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            cachedThreadPool.execute(new Runnable() {

                @Override
                public void run() {
                    System.out.println(index);
                }
            });
        }
    }
}
