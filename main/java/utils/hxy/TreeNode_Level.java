package main.java.utils.hxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author hxy
 * @version V1.0
 * @Package utils.hxy
 * @date 2019/11/6 8:18
 * @Copyright
 */
public class TreeNode_Level {
    Integer level;
    String data;
    TreeNode_Level parent;
    LinkedList<TreeNode_Level> childlist;

    TreeNode_Level(String data ,Integer level) {
        data = data;
        childlist = new LinkedList();
        level = level;
    }
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    TreeNode_Level() {
        data = null;
        childlist = new LinkedList();
        parent = null;
    }

    // 递归读取一棵树
    private ArrayList<TreeNode_Level> displayTree(TreeNode_Level f, int level) {

        ArrayList<TreeNode_Level> result = new ArrayList<>();
             if (f == null) {
            return result;
        }

        for (int i = 0; i < f.childlist.size(); i++) {
            TreeNode_Level obj = f.childlist.get(i);
            obj.level = level;
            result.add(obj);

            if (!obj.childlist.isEmpty()) {
                displayTree(obj, level + 1);
            }
        }

        return result;
    }

    // 排序、按层打印结果
    private void print() {
        TreeNode_Level f = null;   // 到库里取数据

        ArrayList<TreeNode_Level> list = displayTree(f, level = 1);

        Collections.sort(list, new Comparator<TreeNode_Level>() {
            @Override
            public int compare(TreeNode_Level o1, TreeNode_Level o2) {
                // 正序排序
                return o1.getLevel().compareTo(o2.getLevel());
            }
        });

        list.forEach(item -> System.out.println(item.data));
    }


   /* public String print(TreeNode root){
        StringBuilder sb = new StringBuilder();
        if(root != null){
            List<TreeNode> treeNodes = root.getChildren();
            if(treeNodes != null && treeNodes.size() > 0){
                return "";
            }
            sb.append(root.getTexts());
            treeNodes.stream().forEach(i -> sb.append(print(i)));
        }
        return sb.toString();
    } */

    public static void main(String[] args) {
        TreeNode_Level root = new TreeNode_Level("a",0);
        Queue<TreeNode_Level> queue = new LinkedList<>();
        for (queue.offer(root); !queue.isEmpty(); ) {
            TreeNode_Level node = queue.remove();

        }
    }
}
class Node1 {

    String data;
    TreeNode parent;
    LinkedList<Node1> childlist;

    Node1() {
        data = null;
        childlist = new LinkedList();
        parent = null;
    }

    //递归显示并打印一棵树
    private static void displayTree(Node1 f, int level) {

        String preStr = "";     // 打印前缀
        for (int i = 0; i < level; i++) {
            preStr += "    ";
        }

        for (int i = 0; i < f.childlist.size(); i++) {
            Node1 t = f.childlist.get(i);
            System.out.println(preStr + "-" + t.data);

            if (!t.childlist.isEmpty()) {
                displayTree(t, level + 1);
            }
        }
    }

    //递归显示并打印一棵树
    private static ArrayList<Node1> addTree(Node1 f) {
        ArrayList<Node1>  nodes = new ArrayList<Node1>();
        String preStr = "";     // 打印前缀
        if(f != null && f.childlist.isEmpty()){
            for (int i = 0; i < f.childlist.size(); i++) {
                nodes.add(f.childlist.get(i));
            }
        }
        return nodes;
    }
    private static void displayTree(Node1 f) {
        ArrayList<Node1>  nodes = new ArrayList<Node1>();
        if(f != null){
            nodes = addTree(f);
        }
        for (int i = 0; i < nodes.size(); i++) {
            System.out.println(nodes.get(i).data);
        }

    }

}
