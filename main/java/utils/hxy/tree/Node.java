package main.java.utils.hxy.tree;

import java.util.logging.Logger;

/**
 * 树的节点定义
 */

public class Node {
     Node left;
     Node right;
     int data;
    public Node(int data){
        Logger.getLogger ("Node.class");
        this.data = data;
    }
    //打印节点内容
    public void display(){
        System.out.println(data);
    }
}

