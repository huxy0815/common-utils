package main.java.utils.hxy.tree;

/**
 * 树的定义方法
 */
public interface Tree<T extends Comparable> {
    //插入方法
    public boolean insert(int data);
    //查找节点
    public Node find(int key);
    //删除方法
    public boolean delete(int key);
    public Node findMax();
    public Node findMin();
    //中序遍历 左子树——》根节点——》右子树
    public void infixOrder(Node current);
    //前序遍历 根节点——》左子树——》右子树
    public void preOrder(Node current);
    //后序遍历 :左子树——》右子树——》根节点
    public void postOrder(Node current);
    /**
     * 判空
     * @return
     */
    boolean isEmpty();

    /**
     * 二叉树的结点个数
     * @return
     */
    int size();

    /**
     * 返回二叉树的高度或者深度,即结点的最大层次
     * @return
     */
    int height();
}
