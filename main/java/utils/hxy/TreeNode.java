package main.java.utils.hxy;

import java.util.List;

/**
 * @author hxy
 * @version V1.0
 * @Package utils.hxy
 * @date 2019/11/5 16:01
 * @Copyright
 */
public interface TreeNode<T extends  TreeNode<T>> {
    List<T> getChildren();
    String getTexts();
}