package main.java.utils.hxy;

/**
 * @author hxy
 * @version V1.0
 * @Package utils.hxy
 * @date 2019/10/23 14:10
 * @Copyright
 */
public interface PersonCallback {

    void callback(Person person);
}
